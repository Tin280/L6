class ROT13 {
  static encode(inputText) {
    let cipherText = "";
    const shiftAmt = 13;
    for (let i = 0; i < inputText.length; i++) {
      const charCode = inputText.codePointAt(i);
      let newCharPosition;
      if (48 <= charCode && charCode <= 57) {
        newCharPosition = (charCode - 48 + shiftAmt) % 10 + 48;
      }
      else if (65 <= charCode && charCode <= 90) {
        newCharPosition = (charCode - 65 + shiftAmt) % 26 + 65;

      }
      else if (97 <= charCode & charCode <= 122) {
        newCharPosition = (charCode - 97 + shiftAmt) % 26 + 97;
      }
      else {
        newCharPosition = charCode;
      }
      cipherText += String.fromCodePoint(newCharPosition);;
    }

    console.log("Original string: " + inputText);
    console.log("Encoded: " + cipherText);
    return cipherText;
  }
  static decode(a) {
    let inputText = a;
    let outputText = "";
    const shiftAmt = 13;
    for (let i = 0; i < inputText.length; i++) {
      const charCode = inputText.codePointAt(i);
      let newCharPosition
      if (48 <= charCode && charCode <= 57) {
        newCharPosition = (charCode - 48 + shiftAmt) % 10 + 48;
      }
      else if (65 <= charCode && charCode <= 90) {
        newCharPosition = (charCode - 65 + shiftAmt) % 26 + 65;

      }
      else if (97 <= charCode & charCode <= 122) {
        newCharPosition = (charCode - 97 + shiftAmt) % 26 + 97;
      }
      else {
        newCharPosition = charCode;
      }
      outputText += String.fromCodePoint(newCharPosition);
    }

    console.log("Decoded: " + outputText);
    return outputText;
  }
}

let a = ROT13.encode("Hello World");
console.log(a);
ROT13.decode(a);
console.log("\nProgram ends.");
module.exports = { ROT13 };