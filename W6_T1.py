CHARSET_UPPER ="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
CHARSET_LOWER = CHARSET_UPPER.lower()
class ROT13:

    @staticmethod
    def encode(inputText)->None:
        cipherText = ""  
        shiftAmt =13
        for char in inputText:
            charPosition = ord(char)
            if 48 <= charPosition <= 57:
                newCharPosition = (charPosition - 48 + shiftAmt) % 10 + 48
            elif 65 <= charPosition <= 90:
                newCharPosition = (charPosition - 65 + shiftAmt) % 26 + 65
            elif 97 <= charPosition <= 122:
                newCharPosition = (charPosition - 97 + shiftAmt) % 26 + 97
            else:
                newCharPosition = charPosition
            cipherText += chr(newCharPosition)
        print("Original string: " + inputText)
        print("Encoded: "+ cipherText)
        return cipherText

    @staticmethod
    def decode(a,shiftAmt=13) ->None:
        inputText = a
        outputText = ""
        for char in inputText:
            charPosition = ord(char)
            if 48 <= charPosition <= 57:
                newCharPosition = (charPosition - 48 - shiftAmt) % 10 + 48
            elif 65 <= charPosition <= 90:
                newCharPosition = (charPosition - 65 - shiftAmt) % 26 + 65
            elif 97 <= charPosition <= 122:
                newCharPosition = (charPosition - 97 - shiftAmt) % 26 + 97
            else:
                newCharPosition = charPosition
            outputText += chr(newCharPosition)
        
        print("Decoded: " +outputText)
        return outputText
def main():
    print("Starting the program.\n")

    r=ROT13()
    inputText = input("Give a string: ")
    a =r.encode(inputText)  
    r.decode(a)
    print("\nProgram ends.")


# temporary for testingcg-pytest run --set-pythonpath TW6_T1.py -- --tb=no -v.
if __name__ =="__main__":
    main()