/** 
 * This program is menu-driven counter.
 * Menu options are: "Print instructions", "Add count" and "Read count".
 * Counter core-functionality is wrapped into "Counter" class.
 * Menu is it's own function
 * Main program is called "main" and it is a function.
 * Main is start with call `if __name__ == main:` and then call main.
 * File ends here: eof
 */

 const GRAVITY = 9.81;

 class Counter {
     
     _count = 0
 
     static printInstructions() {
         console.log("Instructions below:");
         console.log("1) Print instructions");
         console.log("2) Add count");
         console.log("3) Read count");
         console.log("0) Exit");
         return undefined;
     }
     
     addCount() {
         this._count = this._count + 1;
         return undefined;
     }
 
     readCount() {
         console.log("Count: " + this._count.toString());
         return undefined; // doing the print inside this method
     }
 }
 const readline = require('readline');
 const ask = async (question) => new Promise(
     resolve => {
         const rl = readline.createInterface(process.stdin, process.stdout);
         rl.question(
             question,
             (input) => {
                 rl.close();
                 resolve(input);
             }
         )
     }
 )
 
 const menu = async () => {
     console.log("Counter options:");
     console.log("1) Print instructions");
     console.log("2) Add count");
     console.log("3) Read count");
     console.log("0) Exit");
     const choice = await ask("Your choice: ");
     return choice;
 }
 
 const main = async () => {
     console.log("Program starting.")
     const counter1 = new Counter()
     const choice = await menu();
     console.log(choice);
     // while (true) {
     //    console.log(1);
         //     option = menu()
         //     if option == 1:
         //         Counter.printInstructions()
         //     elif option == 2:
         //         counter1.addCount()
         //     elif option == 3:
         //         counter1.readCount()
         //     elif option == 0:
         //         break
         //     else:
         //         print("Unknown option.")
         // 
     // }
     console.log("Program ends.")
 }
 
 main();
 // eof
 