""""
This program is menu-driven counter.
Menu options are : "Print instructions", "Add count" and "Read count".
Counter funtionality is wapped into "Counter" class.
Menu is it's own funtion
Main program is called "main" and it is a funtion.
Main is start with call `if __name__ == main: `adn then call main.
File ends here: eof

"""

GRAVITYY = 9.81

class Counter:
    
    _count =0

    @staticmethod
    def printInstructions()->None:
        print("Instruction below:")
        print("1) Print instructions")
        print("2) Add count")
        print("3) Read count")
        print("0) Exit")
        return None

    def addCount(self) ->None:
        self._count =self._count + 1
        return None
    def readCount(self) ->None:
        print("Count:" + str(self._count))
        return None #doing the print inside this method

def menu():
    print("Counter options:")
    print("1) Print instructions")
    print("2) Add count")
    print("3) Read count")
    print("0) Exit")
    choice = int(input("Your choice: "))
    return choice

def main():
    print("Program starting.")
    counter1 =Counter()
    while True:
        option =menu()
        if option == 1:
            print("doo something1")
            Counter.printInstructions()
        elif option == 2:
            print("doo something2")
            counter1.addCount()
        elif option== 3:
            print("doo something3")
            counter1.readCount()
        elif option == 0:
            break
        else:
            print("Unknown option.")
    print("Program starting.")



if __name__ =="__main__":
    main()
# eof